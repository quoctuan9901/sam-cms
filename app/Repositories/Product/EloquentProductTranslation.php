<?php

namespace App\Repositories\Product;

use App\Models\ProductTranslation;
use App\Repositories\AbstractTranslationRepository;

class EloquentProductTranslation extends AbstractTranslationRepository implements ProductTranslationRepository
{
    protected $model;

    /**
     * EloquentProductTranslation constructor.
     * @param ProductTranslation $model
     * @author Quốc Tuấn <contact.quoctuan@gmail.com>
     */
    public function __construct(ProductTranslation $model)
    {
        $this->model = $model;
    }
}
