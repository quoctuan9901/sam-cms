<?php

namespace App\Repositories\Product;

use App\Repositories\AbstractTranslationInterface;

interface ProductTranslationRepository extends AbstractTranslationInterface
{

}
