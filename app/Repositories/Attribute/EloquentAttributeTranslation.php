<?php

namespace App\Repositories\Attribute;

use App\Models\AttributeTranslation;
use App\Repositories\AbstractTranslationRepository;

class EloquentAttributeTranslation extends AbstractTranslationRepository implements AttributeTranslationRepository
{
    protected $model;

    /**
     * EloquentAttributeTranslation constructor.
     * @param AttributeTranslation $model
     * @author Quốc Tuấn <contact.quoctuan@gmail.com>
     */
    public function __construct(AttributeTranslation $model)
    {
        $this->model = $model;
    }
}
