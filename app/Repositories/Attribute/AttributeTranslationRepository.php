<?php

namespace App\Repositories\Attribute;

use App\Repositories\AbstractTranslationInterface;

interface AttributeTranslationRepository extends AbstractTranslationInterface
{

}
