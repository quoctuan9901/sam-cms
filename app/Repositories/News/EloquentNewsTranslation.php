<?php

namespace App\Repositories\News;

use App\Models\NewsTranslation;
use App\Repositories\AbstractTranslationRepository;

class EloquentNewsTranslation extends AbstractTranslationRepository implements NewsTranslationRepository
{
    protected $model;

    /**
     * EloquentNewsTranslation constructor.
     * @param NewsTranslation $model
     * @author Quốc Tuấn <contact.quoctuan@gmail.com>
     */
    public function __construct(NewsTranslation $model)
    {
        $this->model = $model;
    }
}
