<?php

namespace App\Repositories\News;

use App\Repositories\AbstractInterface;

interface NewsRepository extends AbstractInterface
{
    /**
     * Get new position in news
     * @return int
     * @author Quốc Tuấn <contact.quoctuan@gmail.com>
     */
    public function getNewPosition(): int;

    /**
     * Get all data news with pivot category
     * @return object
     * @author Quốc Tuấn <contact.quoctuan@gmail.com>
     */
    public function getAllNews(): object;

    /**
     * Get data news with uuid
     * @param string $uuid
     * @return array
     * @author Quốc Tuấn <contact.quoctuan@gmail.com>
     */
    public function getNewsUuid(string $uuid): array;
}
