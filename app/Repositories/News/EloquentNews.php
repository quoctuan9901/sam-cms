<?php

namespace App\Repositories\News;

use App\Models\News;
use App\Repositories\AbstractRepository;

class EloquentNews extends AbstractRepository implements NewsRepository
{
    protected $model;

    /**
     * EloquentNews constructor.
     * @param News $model
     * @author Quốc Tuấn <contact.quoctuan@gmail.com>
     */
    public function __construct(News $model)
    {
        $this->model = $model;
    }

    /**
     * Get new position in news
     * @return int
     * @author Quốc Tuấn <contact.quoctuan@gmail.com>
     */
    public function getNewPosition(): int
    {
        $position = $this->model->max('position');

        return ($position == null) ? 1 : $position + 1;
    }

    /**
     * Get all data news with pivot category
     * @return object
     * @author Quốc Tuấn <contact.quoctuan@gmail.com>
     */
    public function getAllNews(): object
    {
        return $this->model->with('category')->orderBy('created_at', 'DESC');
    }

    /**
     * Get data news with uuid
     * @param string $uuid
     * @return array
     * @author Quốc Tuấn <contact.quoctuan@gmail.com>
     */
    public function getNewsUuid(string $uuid): array
    {
        $news     = $this->model->where('uuid', $uuid)->first();
        $category = $news->category_news()->pluck('category_id')->toArray();
        $images   = $news->news_images()->select('image','alt','position')->get();

        return [
            'news'     => $news,
            'category' => $category,
            'images'   => $images
        ];
    }
}
