<?php

namespace App\Repositories\Category;

use App\Models\CategoryTranslation;
use App\Repositories\AbstractTranslationRepository;

class EloquentCategoryTranslation extends AbstractTranslationRepository implements CategoryTranslationRepository
{
    protected $model;

    /**
     * EloquentCategoryTranslation constructor.
     * @param CategoryTranslation $model
     * @author Quốc Tuấn <contact.quoctuan@gmail.com>
     */
    public function __construct(CategoryTranslation $model)
    {
        $this->model = $model;
    }
}
