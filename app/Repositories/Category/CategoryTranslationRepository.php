<?php

namespace App\Repositories\Category;

use App\Repositories\AbstractTranslationInterface;

interface CategoryTranslationRepository extends AbstractTranslationInterface
{

}
