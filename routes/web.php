<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::any('/ckfinder/connector', '\CKSource\CKFinderBridge\Controller\CKFinderController@requestAction')->name('ckfinder_connector');
Route::any('/ckfinder/browser', '\CKSource\CKFinderBridge\Controller\CKFinderController@browserAction')->name('ckfinder_browser');

Route::get('/', function () {
    $data['config'] = DB::table('configs')->pluck('value', 'attribute')->toArray();
    // dd(file_get_contents(base_path("route/admin.php")));
    return view('welcome',$data);
})->name('index');

Route::get('/orderId/{orderid}',function () {
    $data['config'] = DB::table('configs')->pluck('value', 'attribute')->toArray();
    return view('welcome',$data);
})->name('orderId');
Route::get('/payment_success', 'TestPayment@getStatusCode')->name('pay_success');

Route::post('/created_order','TestPayment@createOrder')->name('created_order');
